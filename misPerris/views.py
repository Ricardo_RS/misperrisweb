from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from .models import Persona,Perros,Admin,Estado
from django.core.mail import send_mail
from django.conf import settings

from django.contrib.admin.views.decorators import staff_member_required
# Create your views here.

#importar user
from django.contrib.auth.models import User
#sistema de autenticación 
from django.contrib.auth import authenticate,logout, login as auth_login

from django.contrib.auth.decorators import login_required

def index(request):
    user = User.objects.get(id=2)
    return render(request,'index.html',{'Perros':Perros.objects.all(),'usuario':user})

def login(request):
    return render(request,'login.html',{})

def recuperacion(request):
    return render(request,'recuperacion.html',{})


def registro(request):
    return render(request,'registro.html',{})


def nuevapass(request):
    return render(request,'nuevapass.html',{})


    
@login_required(login_url='login')
@staff_member_required(login_url='index')
def adminp(request):
    return render(request,'adminp.html',{})

@login_required(login_url='login')
@staff_member_required(login_url='index')
def adopcion(request):
    return render(request,'adopcion.html',{'Perros':Perros.objects.all()})

@login_required(login_url='login')   
@staff_member_required(login_url='index')
def editor(request):
    return render(request,'editor.html',{})



#Metodos para las personas

def crearPersona(request):
    nombre = request.POST.get('nombre','') 
    correo = request.POST.get('correo','') 
    contrasenia = request.POST.get('contrasenia','') 
    rut= request.POST.get('rut','') 
    fechaN= request.POST.get('fechaN','')
    numeroC= request.POST.get('numeroC',0)
    tipoV= request.POST.get('numeroC','')
    region= request.POST.get('region','')
    comuna= request.POST.get('comuna','')
    persona=Persona(nombre=nombre,correo=correo,contrasenia=contrasenia,rut=rut,fechaN=fechaN,numeroC=numeroC,tipoV=tipoV,region=region,comuna=comuna)
    persona.save()

    user=User.objects.create_user(username=persona.correo,password=persona.contrasenia)
    user.save()

    subject = 'Gracias pro registrarte en mi sitio ;D'
    message = nombre+'  tu contraseña es'+contrasenia
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [persona.correo,]
    send_mail( subject, message, email_from, recipient_list )

    
    return redirect('index')

    #Metodo para validacion de login de usuarios

def iniciarSession(request):
    correo = request.POST.get('correo','')
    contrasenia = request.POST.get('contrasenia','')
    user = authenticate(request,username=correo, password=contrasenia)



    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect("index")
    else:
        return redirect("login")
    
@login_required(login_url='login')
def cerrarSession(request):
    logout(request)
    return redirect('index')


    
@login_required(login_url='login')
def editarPersona(request,id):
    persona=Persona.objects.get(pk=id)
    nombre = request.POST.get('nombre','') 
    correo = request.POST.get('correo','') 
    contrasenia = request.POST.get('contrasenia','') 
    rut = request.POST.get('rut','') 
    fechaN = request.POST.get('fechaN','')
    numeroC = request.POST.get('numeroC',0)
    tipoV=  request.POST.get('tipoV','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    persona.nombre = nombre
    persona.correo = correo
    persona.contrasenia = contrasenia
    persona.rut = rut
    persona.fechaN = fechaN
    persona.numeroC = numeroC
    persona.tipoV = tipoV
    persona.region = region
    persona.comuna = comuna
    persona.save()
    return redirect('index')

@login_required(login_url='login')
def eliminarPersona(request,id):
    persona=Persona.objects.get(pk=id)
    persona.delete()

#metodos admin
@login_required(login_url='login')
def crearAdmin(request):
    nombre = request.POST.get('nombre','') 
    correo = request.POST.get('correo','') 
    contrasenia = request.POST.get('contrasenia','') 
    rut= request.POST.get('rut','') 

    administador=Admin(nombre=nombre,rut=rut,correo=correo,contrasenia=contrasenia)
    administador.save()

    user=User.objects.create_user(username=administador.correo,password=administador.contrasenia,is_staff=True)
    user.save()

    return redirect('index')
@login_required(login_url='login')
def eliminarAdmin(request,id):
    administador=Admin.objects.get(pk=id)
    administador.delete()

@login_required(login_url='login')
def editarAdmin(request,id):
    administador=Admin.objects.get(pk=id)
    nombre = request.POST.get('nombre','') 
    correo = request.POST.get('correo','') 
    contrasenia = request.POST.get('contrasenia','') 
    rut= request.POST.get('rut','') 
    administador.nombre=nombre
    administador.correo=correo
    administador.contrasenia=contrasenia
    administador.rut=rut
    administador.save()
    return redirect('index')

#Metodos para los perretes
@login_required(login_url='login')
def agregarPerro(request):
    nombre=request.POST.get('nombre','')
    raza=request.POST.get('raza','')
    descripcion=request.POST.get('descripcion','')
    estado=request.POST.get('estado','')
    foto=request.FILES.get('foto',False)
    perr=Perros(nombre=nombre,raza=raza,descr=descripcion,estado=estado,foto=foto)
    perr.save()
    return redirect('adopcion')

@login_required(login_url='login')
def eliminarPerro(request,id):
    perr=Perros.objects.get(pk=id)
    perr.delete()
    return redirect('adopcion')



@login_required(login_url='login')
def editarAdopcion(request,id):
    perro = Perros.objects.get(pk=id)
    return render(request,'editor.html',{'perro':perro})

@login_required(login_url='login')
def modificadoAdopcion(request,id):
    perro = Perros.objects.get(pk=id)

    nombre=request.POST.get('nombre','')
    raza=request.POST.get('raza','')
    descripcion=request.POST.get('descripcion','')
    estado=request.POST.get('estado','')
    foto=request.FILES.get('foto',False)

   
    perro.nombre = nombre
    perro.raza = raza
    perro.descr = descripcion
    perro.estado = estado
    perro.foto = foto
    perro.save()
    return HttpResponse("nombre : "+perro.nombre+" apellido: "+perro.raza)

#metodos para llenados de combobox

#estado del perro para los rescates
@login_required(login_url='login')
def agregarEstado(request):
    estd=request.POST.get('estado','')
    estado=Estado(disp=estd)






def recuperacionContra(request):
    correo=request.POST.get('correo','')
    for p in Persona.objects.all():
        if (p.correo == correo):
            persona = Persona.objects.get(id=p.id)
    for u in User.objects.all():
        if (u.username == correo):
            user = User.objects.get(id=u.id)

    return render(request,'nuevapass.html',{'Persona':persona,'User':user})

def modificadoContra(request,id):
    persona = Persona.objects.get(pk=id)
    user = User.objects.get(username=persona.correo)

    contrasenia=request.POST.get('contrasenia','')

    persona.contrasenia = contrasenia
    user.set_password(contrasenia)
    persona.save()
    user.save()

    subject = 'Thank you for registering to our site'
    message = persona.contrasenia
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [persona.correo,]
    send_mail( subject, message, email_from, recipient_list )

    return HttpResponse("nueva contraseña : "+persona.contrasenia)

def profileview(request):

    return redirect('index')
