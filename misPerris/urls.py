from django.conf import settings
from django.conf.urls.static import static

from django.contrib.auth.decorators import permission_required

from django.urls import path,include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('adopcion',views.adopcion,name='adopcion'),
    path('login',views.login,name="login"),
    path('registro',views.registro,name="registro"),
    path('editor',views.editor,name="editor"),
    path('recuperacion',views.recuperacion,name="recuperacion"),
    path('recupera',views.recuperacionContra,name="recupera"),
    path('nuevapass',views.nuevapass,name="nuevapass"),
    path('nuevapass/<int:id>',views.modificadoContra,name="cambiapass"),

    path('adopcion/crear',views.agregarPerro,name="nuevadopcion"),
    path('adopcion/eliminar/<int:id>',views.eliminarPerro,name="eliminar"),

    path('registro/crear',views.crearPersona,name="crear"),
    path('registro/eliminar/<int:id>',views.eliminarPersona,name="eliminar"),


    path('editor/seleccion/<int:id>',views.editarAdopcion,name="seleccion"),
    path('editor/modificacion/<int:id>',views.modificadoAdopcion,name="editar"),

   
    
    path('adminp',views.adminp,name="adminp"),
    path('adminp/crear',views.crearAdmin,name="creara"),
    path('login/iniciar',views.iniciarSession,name="iniciarSession"),
    path('cerrarsession',views.cerrarSession,name="cerrarSession"),
    path('accounts/profile/', views.profileview,name="correcto"),

    path('accounts/', include('allauth.urls')),




] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)