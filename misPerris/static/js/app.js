console.log("HELLLO")

function ColorMenu() {
var list = document.getElementsByClassName("row")[0]
var tamanio = document.getElementById('BodyPage').clientWidth;
if (tamanio<1000) {
console.log(tamanio)

list.getElementsByClassName("colormenu")[0].style.backgroundColor = "rgb(109, 204, 170)";
list.getElementsByClassName("colormenu")[1].style.backgroundColor = "rgb(109, 204, 170)";
list.getElementsByClassName("colormenu")[2].style.backgroundColor = "rgb(109, 204, 170)";
list.getElementsByClassName("colormenu")[3].style.backgroundColor = "rgb(109, 204, 170)";
list.getElementsByClassName("colormenu")[4].style.backgroundColor = "rgb(109, 204, 170)";
list.getElementsByClassName("colormenu")[5].style.backgroundColor = "rgb(109, 204, 170)";
list.getElementsByClassName("colormenu")[6].style.backgroundColor = "rgb(109, 204, 170)";

} else {

list.getElementsByClassName("colormenu")[0].style.backgroundColor = "#85D4B7";
list.getElementsByClassName("colormenu")[1].style.backgroundColor = "#85D4B7";
list.getElementsByClassName("colormenu")[2].style.backgroundColor = "#85D4B7";
list.getElementsByClassName("colormenu")[3].style.backgroundColor = "#85D4B7";
list.getElementsByClassName("colormenu")[4].style.backgroundColor = "#85D4B7";
list.getElementsByClassName("colormenu")[5].style.backgroundColor = "#85D4B7";
list.getElementsByClassName("colormenu")[6].style.backgroundColor = "#85D4B7";
}
}


/*solo letras*/


$("#nombreCompleto").on('keyup', function(e) {
    var val = $(this).val();
   if (val.match(/[^a-zA-Z ñ Ñ á é í ó ú]/g)) {
       $(this).val(val.replace(/[^a-zA-Z ñ Ñ á é í ó ú]/g, ''));
   }
});






/* Funcion para la validacion de datos ingresadas */



    $("#regForm").validate({
        rules:{
            correo:{
                required:true,
                email: true,
            },

            nombre:{
                required: true,
            },

            fechaNa:{
                required: true,
            },

            numeroContacto:{
               required:true,
               pattern:true,
            },

            rut:{
                required:true,   
            },

            opcionesCasas:{
                required:true,
            },
            region:{
                required:true,
            },
            comuna:{
                required:true,
            },

            passwrd:{
                required:true
            },

            repswd:{
                    required:true,
                    equalTo:"#passwrd"
            }

        },
        messages:{
            correo:{
                required: "Debe ingresar un correo",
                email: "El formato del correo ingresado no corresponde",
            },

            nombreCompleto:{
                required: "Debe ingresar su nombre",
            },

            fechaNa:{
                required: "Debe ingresar una fecha de nacimiento",
            },

            numeroContacto:{
               required:"Debe ingresar un numero de contacto",
               pattern:"Numero invalido",
            },

            rut:{
                required:"Debe ingresar su rut" ,  
            },

            opcionesCasas:{
                required:"Seleccione un tipo de casa",
            },           
             region:{
                required:"Selecciona una region",
            },
            comuna:{
                required:"Selecciona una comuna",
            },

            passwrd:{
                required: "Debe ingresar contraseña"
            },

            repswd:{
                required:"Debe ingresar la misma contraseña",
                equalTo: "Ingrese algo "
            }

        },



    })

    var input = document.querySelector('input');

    input.addEventListener('input', function()
{
 templateerror()
});

$("#regForm").submit(function(){
    if(Fn.validaRut($("#rut").val())){

        console.log("RUT CORRECTO")
        return true;
    }else{
        console.log("RUT INCORRECTO")
        let rut = document.getElementById('error');
        rut.innerHTML='Rut Ingresado es incorrecto'; 
        return false;
    }
    templateerror()

  
})

function templateerror(){


    let mail = document.getElementById('textmail');
    let rut = document.getElementById('textrut');
    let name = document.getElementById('textname');
    let date = document.getElementById('textdate');
    let psw = document.getElementById('textpsw');
    let num = document.getElementById('textnum');
    let repass = document.getElementById('repasstext');
    mail.innerHTML=' '; 
    rut.innerHTML=' '; 
    name.innerHTML=' '; 
    date.innerHTML=' ';
    psw.innerHTML=' ';
    num.innerHTML=' ';
    repass.innerHTML=' ';
}

var Fn = {
    validaRut : function (valida) {
        if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( valida ))
            return false;
        var tmp 	= valida.split('-');
        var digv	= tmp[1]; 
        var rut 	= tmp[0];
        if ( digv == 'K' ) digv = 'k' ;
        return (Fn.dv(rut) == digv );
    },
    dv : function(T){
        var M=0,S=1;
        for(;T;T=Math.floor(T/10))
            S=(S+T%10*(9-M++%6))%11;
        return S?S-1:'k';
    }
}




/*Comunas y regiones para el formulario */
$(function(){

    regiones.regiones.forEach(region => {
        $("#region").append('<option value="'+region.region+'">'+region.region+'</option>')
    });


})

$("#region").change(function(){
    $("#comuna").html("")
    var region = $(this).val()
    var comunas = regiones.regiones.find(item => item.region == region);
    $("#comuna").append('<option hidden >Seleccione</option>')
    comunas.comunas.forEach(comuna => {
        $("#comuna").append('<option value="'+comuna+'">'+comuna+'</option>')
    });
})

var regiones = {
    "regiones": [{
        "region": "Arica y Parinacota",
        "comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
    },
    {
        "region": "Tarapacá",
        "comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
    },
    {
        "region": "Antofagasta",
        "comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
    },
    {
        "region": "Atacama",
        "comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
    },
    {
        "region": "Coquimbo",
        "comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
    },
    {
        "region": "Valparaíso",
        "comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
    },
    {
        "region": "Región del Libertador Gral. Bernardo O’Higgins",
        "comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
    },
    {
        "region": "Región del Maule",
        "comunas": ["Talca", "Constitución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "Retiro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
    },
    {
        "region": "Región de Ñuble",
        "comunas": ["Cobquecura", "Coelemu", "Ninhue", "Portezuelo", "Quirihue", "Ránquil", "Treguaco", "Bulnes", "Chillán Viejo", "Chillán", "El Carmen", "Pemuco", "Pinto", "Quillón", "San Ignacio", "Yungay", "Coihueco", "Ñiquén", "San Carlos", "San Fabián", "San Nicolás"]
    },
    {
        "region": "Región del Biobío",
        "comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío"]
    },
    {
        "region": "Región de la Araucanía",
        "comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria"]
    },
    {
        "region": "Región de Los Ríos",
        "comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
    },
    {
        "region": "Región de Los Lagos",
        "comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "Frutillar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
    },
    {
        "region": "Región Aisén del Gral. Carlos Ibáñez del Campo",
        "comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
    },
    {
        "region": "Región de Magallanes y de la Antártica Chilena",
        "comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "Antártica", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
    },
    {
        "region": "Región Metropolitana de Santiago",
        "comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "Tiltil", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
        }]
    }















