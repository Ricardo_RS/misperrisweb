from django.db import models
from django.dispatch import receiver
from allauth.account.signals import user_signed_up

# Create your models here.
class Persona(models.Model):
    nombre = models.CharField(max_length=40,null=False,blank=False)
    correo = models.EmailField(max_length=200,null=False,blank=False)
    contrasenia = models.CharField(max_length=40,null=False,blank=False)
    rut= models.CharField(max_length=50,null=False,blank=False)
    fechaN=models.CharField(max_length=10)
    numeroC=models.IntegerField()
    tipoV=models.CharField(max_length=70,null=False,blank=False)
    region=models.CharField(max_length=100,null=False,blank=False)
    comuna=models.CharField(max_length=1001,null=False,blank=False)
    
    def __str__(self):
        return "PERSONA"      
   

class Admin(models.Model):
    nombre=models.CharField(max_length=60,null=False,blank=False)
    rut=models.CharField(max_length=90,null=False,blank=False)
    contrasenia=models.CharField(max_length=90,null=False,blank=False)
    correo=models.EmailField(max_length=200,null=False,blank=False)
    class Meta:
        permissions = (
            # Permission identifier     human-readable permission name
            ("can_AddFoto",               "Can Add Foto"),)


class Perros(models.Model):
    nombre=models.CharField(max_length=60,null=False,blank=False)
    raza=models.CharField(max_length=80,null=False,blank=False)
    descr=models.CharField(max_length=100,null=False,blank=False)
    estado=models.CharField(max_length=30,null=False,blank=False)
    foto = models.ImageField(upload_to="fotos/")
    def __str__(self):
        return "PERROS"


class Estado(models.Model):
    disp=models.CharField(max_length=30,null=False,blank=False)

"""class Vivienda(models.Model):
    tipo=models.CharField(max=80,null=False)"""


@receiver(user_signed_up)
def create_user_profile(request, user, **kwargs):
    profile = Profile.objects.create(user=user)
    profile.save()